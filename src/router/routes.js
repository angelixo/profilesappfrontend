
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue'), },
      { path: '/favoritos', component: () => import('pages/Favoritos.vue'), },
      { path: '/listas', component: () => import('pages/Listas.vue'), }

    ]
  },
  {
    path: '/getList',
    compoonent: () => {return 'hola'},
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
