/**
 * Creamos APIs y las pasamos como prototype para 
 * poder usarlas donde sea necesario.
 */
import Vue from 'vue'
import axios from 'axios'

Vue.prototype.$axios = axios

const randomUserAPI = axios.create({ 
  baseURL: 'https://randomuser.me/api'
 })
Vue.prototype.$random_user_api = randomUserAPI

const backend = axios.create({ 
    baseURL: 'http://localhost:4000/api',
})
Vue.prototype.$backend = backend


export { axios, randomUserAPI, backend }