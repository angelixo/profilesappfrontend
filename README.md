# Pwa (pwa)

PWA para la gestión de usuarios de randomuser (Api). La lee los usuarios, te permite (junto con el backend) guardar favoritos, exportarlos y crear listas. 

### Descargar el código

``` git clone https://gitlab.com/angelixo/profilesappfrontend.git```
## Instalación con Docker Compose


``` docker-compose up ```

## Instrucciones para instalacion mediante YARN, la cual requiere tener instalado Quasar Framework
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://v1.quasar.dev/quasar-cli/quasar-conf-js).
